console.log("Hello World from s15!");

let numString1 = "5";
let numString2 = "10";
let num1 = 4;
let num2 = 6;
let num3 = 1.5;
let num4 = .5;

console.log(numString1);
console.log(numString2);
console.log(num1);
console.log(num2);
console.log(num3);
console.log(num4);

console.log(num1+num2);//10

	/*with the use of + (addition operator), we can return a 
	value from addition between two numnber types. We can also save
	the returned value in a variable.*/

	let sum1 = num1+num2;
	console.log(sum1);

	let numString3 = numString1 + sum1;
	console.log(numString3);

	let sampleStr = "Charles";
	console.log(sampleStr + num2);

	//Subtraction Operator(-)

	// allows us to subtract the operants and result to a difference.
	//it returns a number that we can save in a variable.


	let difference1 = num1 - num3;
	console.log(difference1);

	/*subtraction operator, the string is converted to a number and subtracted*/

	let difference2 = numString2 - num2;
	console.log(difference2);

	let sampleStr2 = "Joseph";

	let difference3 = sampleStr2 - num1;
	console.log(difference3);//NaN -not a number.

	let difference4 = numString2 - numString1;
	console.log(difference4);




function subtract(num1,num2){
	return num1 - num2;

}

let difference5 = subtract(25,5)
console.log(difference5)


//multiplication operator (*)

//Multiply both operands and return the products as value.
//Convert any string to number befroe multiplying

let product1 = num1*num2;
console.log(product1);//24 - number

let product2 = numString1 * numString2;
console.log(product2);//50 - number

/* Division operator (/) */

//It will allow us to divide right operand from the left operand.
	// leftOperand/righrtOperand

	let quotient1 = num2/num4;
	console.log(quotient1);//12 - number type

	let quotient2 = numString2/numString1;
	console.log(quotient2);//2 - number type



function multiply(num1,num2){
	return num1*num2;
}

let product3 = multiply(4,5);
console.log(product3);




function divide(num1,num2){
	return num1/num2;
}

let quotient3 = divide(20,5);
console.log(quotient3);

console.log(product3*0);
console.log(quotient3/0);//infinity is special type of number because we cannot divide a
//number by zero



console.log(5%5);
//leftOperand%rightOperand = modulo or the remainder 
console.log(25%6);
console.log(50%2);

let modulo1 = 5%6;
console.log(modulo1);


//Assignment Operators
	//Basic Assignment operator (=)


	let variable = "initial value";
	variable = "new value";
	console.log(variable);//new value

	let sample1 = "sample value";
	variable = sample1;
	console.log(variable);//sample value
	console.log(sample1);

	let sample2 = "new sample value";
	variable = sample2;
	console.log(variable);
	console.log(sample2);

	
	/* constant's value cannot iupdated or re-assigned*/
	//const pi = 3.1416;
	//pi = 5000;
	//console.log(pi);

	/*function displayDetails (){

		let details = {
			name: name,
			password: password,

		}

		return details
	}
	*/


	let sum2 = 10;
	sum2 += 20;
	console.log(sum2);//30

	let sum3 = 5;
	sum3 += 5;
	console.log(sum3);//10


	//when using addition assignment operator, the left hand operand
	//operand should be a variable.
	//console.log(5+=5);

	let sum4 = 30;
	sum4+="Curry";
	console.log(sum4);

	//30 + "curry" = 30 will br converted into a string and will be concatenated with the string 
	//"curry"
	//sum4= "30Curry"


	let sum5 = "50";
	sum5+= "50";
	console.log(sum5);//5050 since both are strings, it concatenated


	let fullName = "Wardell"
	let name = "Stephen"
	fullName+=name;
	console.log(fullName)//"WardellStephen"
	fullName+="Curry";
	console.log(fullName);//"WardellStephenCurry"
	fullName+="II";
	console.log(fullName);

	//Subtraction Assignment Operator (-=)

	let numSample = 50;
	numSample-=10;
	console.log(numSample); // 40

	let numberString = "100";
	let numberString2 = "50";
	numberString-=numberString2;
	console.log(numberString);//50

	let text = "ChickenDinner";
	text -= "Dinner";
	console.log(text)//NaN

	//Multiplication Assignment Operator

	let sampleNum1 = 3;
	let sampleNum2 = 4;
	sampleNum1*=sampleNum2;
	console.log(sampleNum1);
	console.log(sampleNum2);


	let sampleNum3 = 5;
	let sampleNum4 = "6";
	sampleNum3*=sampleNum4;
	console.log(sampleNum3);//30

	//Division assignment operator

	let sampleNum5 = 70;
	let sampleNum6 = 10;
	sampleNum5/=sampleNum6;
	console.log(sampleNum5);//7

	let sampleNum7 = 45;
	sampleNum7/=0;
	console.log(sampleNum7);//infinty. invalid division


	//Order of operations (MDAS)

	let mdasResult = 1 + 2 - 3 * 4 / 5;
	console.log(mdasResult);

	//PEMDAS = Parenthesis, Exponents, multi,d,a,s 

	let pemdasResult = 1 + (2-3)*(4/5);
	console.log(pemdasResult);


//Increment and Decrement

	let z = 1;
	//pre-fix:
	++z;
	console.log(z);

	//post-fix
	console.log(z++);
	console.log(z);


	z++;
	console.log(z);

	//Prefix vs Postfix
	console.log(++z);//5 increment first and then returned the incremented value
	console.log(z++);//5 previous value is returned first berfore incrementation.
	console.log(z)



//prefix and postfix decrementation

console.log(--z);
console.log(z--);//5 the result of subtraction by 1 is not immediately returned,
	//instead the previous will be returned.
console.log(z);//4- new value is returned.



/// difference is ?????


//console.log(5++);


//incrementation/decrementation are used on variables that contain nubmer types.

let sampleNumString = "5";
console.log(++sampleNumString);// numeric string is converted to number adn will be 
//incremented

let sampleString = "James";
console.log(sampleString++);
/*will convert your string first into a number, since the string contains alphanumeric characters
it is converted to NaN*/


/*Comparison Operators*/



//Comparison Operators are used to compare the values of the left and right operands.
//Comparison Operators return a boolean (true/false)

	//loose equality operator
	//evaluates if the operands have the same value.

		console.log(1==1);

		//can have the result of comparison in a variable.

		let isSame = 55 == 55;
		console.log(isSame);

		console.log(1 == '1');//true- loose equality operator prioritizes
		//the sameness of the value because it enforces Forced Coercion or whewhen the types of the operands
		//are automatically changed before the comparison. the string here was actually converted to number 
		//1 ==1 = true

		console.log(0 == false);//true - with forced coercion, both were converted
		// to numbers. false is a boolean, but converted to number it is 0. that's why the 
		//result is true.

		/*js has a function to allow us to convert data from one type to a number*/

		let sampleConvert = Number(false);
		console.log(sampleConvert);


		let sampleConvert2 = "2500";
		sampleConvert2 = Number(sampleConvert2);
		console.log(sampleConvert2);

		console.log(1 == true);//true - the boolean true when converted to number
		//is 1. So 1 == 1 = true.

		console.log(5 == "5");// true sameness of value, forced coercion

		console.log(true == "true");/*false- forced coercion wherein both operands 
		are converted to numbers. boolean true was converted to a number as 1.
		string "true" was converted to a number nut since it was a word, it resulted 
		to NaN. 1 == NaN is false.*/

		console.log(false == "false");//false - forced coercion. boolean converted false to number 0.
		// "false" is converted to Nan 0 == Nan is false.


		//strict equality operator

		/*evaluates the sameness of both values and types of operands. It's more
		preferred because JS is a loose-typed language.
		*/

		console.log(1 == "1");

		console.log(1 === "1");//false - checks sameness of both values and types
		//of the operands.

		console.log("james2000" === "James2000");/*false = j === J*/

		console.log(55 === "55")//false - cuz it checks sameness of value and type

		//loose Inequality Operator

		// it also does force coercion.

		console.log(1 != "1");//false force coercion converted both to number.

		console.log("James" != "John");//true

		console.log(5 !=55);

		console.log(1500 != "5000");//true

		console.log(true != "true");//true

		//Strict Inequality Operator

		console.log(5 !== 5);//false

		console.log(5 !== "5");//true different types

		console.log(true !== "true");//true

		//Variables

		let nameStr1 = "Juan";
		let nameStr2 = "Jack";
		let numberSample = 50;
		let numberSample2 = 60;
		let numStr1 = "15";
		let numStr2 = "25";

		console.log(numStr1 == 50);//false
		console.log(numStr1 == 15);//true
		console.log(numStr2 === 25);//false
		console.log(nameStr1 != "James");//true
		console.log(numberSample !== "50");//true
		console.log(numberSample != "50");//false
		console.log(nameStr1 == nameStr2);//false
		console.log(nameStr2 === "jack");//false

		//Relational Comparison Operators returns a boolean


		let price1 = 500;
		let price2 =700;
		let price3 = 8000;
		let numStrSample = "5500"


		//greater than (>)
		console.log(price1 > price2);//false
		console.log(price3 > price2);//true
		console.log(price3 > numStrSample);//true becaue of forced coercion

		//less than (<)
		console.log(price2 < price3);//true
		console.log(price1 < price3);//true
		console.log(price3 < 1000);//false
		console.log(numStrSample < price1);//false--- forced coercion


		//greater than or equal to
		console.log(price1 >= 500);//true

		console.log(price3 >= 10000);//false
		console.log(price2 >= 600);//true

		//less than or equal to
		console.log(price2 <= numStrSample);//true
		console.log(price3 <=price1);//false
		console.log(price2 <= 700);//true


		//Logical Operators

		let isAdmin = false;
		let isRegistered = true;
		let isLegalAge = true;

		console.log(isRegistered && isLegalAge);//true

		console.log(isAdmin && isRegistered);//false

		let user1 = {
			username: "perterphoenix_1999",
			age: 28,
			level: 15,
			isGuildAdmin: false
		}

		let user2 = {
			username: "kingBrodie00",
			age: 13,
			level: 50,
			isGuildAdmin: true,
		}

		//To be able to access the properties of an object we use dot 
		//notation:
		console.log(user1.username);//perterphoenix_1999
		console.log(user1.level);//15

		let authorization1 = user1.age >= 18 && user1.level >= 25;
		console.log(authorization1);//false user is unauthorized

		let authorization2 = user2.age >= 18 && user2.level >= 25;
		console.log(authorization2);//false user2 is underage

		//Guild Leaders Meeting
		let authorization3 = user1.level >= 10 && user1.isGuildAdmin === true
		console.log(authorization3)//false user1 is not admin

		let authorization4 = user2.level >= 10 && user2.isGuildAdmin === true
		console.log(authorization4)

		// or || 

		//Or operator returns true if at least one operand results to true.

		//on-sight meeting between new users

		let authorization5 = user1.age >= 18 || user1.level <= 15;
		console.log(authorization5);//true


		//zoom meeting between users/high level users
		let authorization6 = user2.age >= 18 || user2.level >= 1;
		console.log(authorization6);//true

		//Joining a new group with low level
		let authorization7 = user1.level >= 10 || user1.isGuildAdmin === false
		console.log(authorization7);// true

		//Joining a new group
		let authorization8 = user1.level >= 50 || user1.isGuildAdmin === false
		console.log(authorization8);//true

		//NOt operator

		/*
		let isAdmin = false;
		let isRegistered = true;
		let isLegalAge = true;


		*/

		console.log(!isRegistered);//false
		console.log(!isAdmin);//true


		//Conditional Statements

			//A conditional statement is a key feature of a programming language
			//It allows to perform certain tasks based on a condition.
			// sample:
				//Is it windy today? (What do we do?)
				//Ids it Monday today? (What do we do?)

		//If-Else Statements

/*
		let user1 = {
			username: "perterphoenix_1999",
			age: 28,
			level: 15,
			isGuildAdmin: false
		}

		let user2 = {
			username: "kingBrodie00",
			age: 13,
			level: 50,
			isGuildAdmin: true,
		}
*/

		if(user1.age >=18){
			alert("You are allowed to enter!");
		};

		if(user2.age >= 18){
			alert("User2 you are allowed to enter.");
		};

		if(user2.age >= 18){
			alert("User2 you are allowed to enter.")
		} else {
			alert("User2, you are not allowed to enter.")
		}

		if(user1.level >=20){
			console.log("User1 is not a noobie.")
		} else {
			console.log("User1 is a noobie.")
		}

		if(user2.level >= 20){
			console.log("User2 is not a noobie.")
		} else {
			console.log("User2 is a noobie.")
		}


		if(user1.isGuildAdmin === true){
			console.log("Welcome Back, Guild Admin.")
		} else {
			console.log("You are not authorized to enter.")
		}

/*
	else-if

	else-if executes a code blok ifg the previous condition....


*/


	if(user1.level >= 35){
		console.log("Hello, Knight!");
	} else if(user1.level >= 25){
		console.log("Hello Swordsman!");
	} else if(user1.level >= 10){
		console.log("Hello, Rookie!");
	} else {
			console.log("Level out of range.");
		
	}

	if(user2.level >= 35){
		console.log("Hello, Knight!");
	} else if(user2.level >= 25){
		console.log("Hello Swordsman!");
	} else if(user2.level >= 10){
		console.log("Hello, Rookie!");
	} else {
			console.log("Level out of range.");
	}	

	//Logical Operators for If conditions:

	let usernameInput1 = "nicoleIsAwesome100";
	let passwordInput1 = "iamawesomenicole";
	
	let usernameInput2 = ""
	let passwordInput2 = null;

	function register(username,password){

		if(username === "" || password === null){
			console.log("Please complete the form.")
		} else {
			console.log("Thank you for registering.")
		}

	}

	 	/* user1 = {
			username: "perterphoenix_1999",
			age: 28,
			level: 15,
			isGuildAdmin: false
		}

		let user2 = {
			username: "kingBrodie00",
			age: 13,
			level: 50,
			isGuildAdmin: true,
		}

*/

	register(usernameInput1, passwordInput1);
	register(usernameInput2, passwordInput1);

	function requirementChecker(level,isGuildAdmin){

		if(level <= 25 && isGuildAdmin === false){
			console.log("Welcome to the Newbies Guild")
		} else if (level > 25) {
			console.log("You are too strong.")
		} else if (isGuildAdmin === true) {
			console.log("You are a guild admin.")
		}
	}

	requirementChecker(user1.level,user1.isGuildAdmin);//user1 can join
	requirementChecker(user2.level,user2.isGuildAdmin);//user2 cannot


	let user3 = {
		username: "richieBillions",
		age: 20,
		level:20,
		isGuildAdmin: true
	}

	requirementChecker(user3.level,user3.isGuildAdmin);

	function addNum(num1,num2){
		if(typeof num1 === "number" && typeof num2 === "number"){
			console.log("Run only if both arguments passed are number type")
		} else {
			console.log("One or both of the arguments are not numbers.")
		}
	}

	addNum(5,10);//run the if statement because both arguments are numbers.
	addNum("6",20);//run the else because one of the arguments is not a number.

	//tyopeof -used for validating the data type of variables or data.
	//It returns a string after evaluating the data type of the data that comes after it

	let str = "sample"
	console.log(typeof str);

	console.log(typeof 75);


	//Mini-activity

	function dayChecker(day){

		day = day.toLowerCase();
		console.log(day)

		/*if(day === "Sunday"){
			console.log("Today is Sunday; Wear White.")
		} else if(day ==="Monday") {
			console.log("Today is Monday; Wear Blue.")
		} else if(day=== "Tuesday") {
			console.log("Today is Tuesday; Wear Green.")
		} else if(day === Wednesday) {
			console.log("Today is Wednesday; Wear Purple.")
		} else if(day === Thursday) {
			console.log("Today is Thursday; Wear Brown.")
		} else if(day === Friday) {
			console.log("Today is Friday; Wear Red.")
		} else if(day === Saturday) {
			console.log("Today is Saturday; Wear Pink.")
		}
	}*/

	/*syntax: 

		switch(expression/condition){
			case: value:
				statement;
				break;
			default:
				statement;
				break;
		}*/	

	switch(day.toLowerCase()){
		case 'sunday':
		console.log("Today is " + day + "; Wear White");
		break;
		case 'monday':
		console.log("Today is " + day + "; Wear Blue");
		break;
		case 'tuesday':
		console.log("Today is " + day + "; Wear Green");
		break;
		case 'wednesday':
		console.log("Today is " + day + "; Wear Purple");
		break;
		case 'thursday':
		console.log("Today is " + day + "; Wear Brown");
		break;
		case 'friday':
		console.log("Today is " + day + "; Wear Red");
		break;
		case 'saturday':
		console.log("Today is " + day + "; Wear Pink");
		break;

		default:
			console.log("Invalid Input. Enter a valid day of the week.");
	}

}

dayChecker("Tuesday")


/*
	= basic assignment operator
	== Loose Equality
	=== Strict Equality



*/


//Eugene,Vincent,Dennis,Alfred,Jeremiah

//switch which display the power level of a selected member

let member = "Vincent";

switch(member){

	case "Eugene":
		console.log("Your power level is 20000");
		break;
	case "Dennis":
		console.log("Youre power level is 15000");
		break;
	case "Vincent":
		console.log("Your power level is 14500");
		break;
	case "Jeremiah":
		console.log("Your power level is 10000");
		break;
	case "Alfred":
		console.log("Your power level is 8000");
		break;
	default:
		console.log("Invalid Input. Add a Ghost Fighter member.")
	}

function capitalChecker(country){
	switch(country){
		case "philippines":
			console.log("Manila");
			break;
		case "usa":
			console.log("Washington D.C.");
			break;
		case "japan":
			console.log("Tokyo");
			break;
		case "germany":
			console.log("Berlin");
		break;
		default:
			console.log("Input is out of range. Choose another country.")
	}
}

capitalChecker("philippines")


//Ternary Operator

	//Conditional statement as if-else. However it was introduced to be short hand way 
	//to write if-else.

	//Syntax

	//condition ? if-statement : else-statement

	let superHero = "Batman"

	superHero === "Batman" ? console.log("You are rich.") : console.log("Bruce Wayne is richer than you.");

	//Ternary operators require an else statement
/*	superHero === "Superman" ? console.log("Hi, Clark!");*/

	//Ternary operators can implicitly return value even without the return keyword.

	let currentRobin = "Tim Drake";

	let isFirstRobin = currentRobin === "Dick Grayson" ? true : false
	console.log(isFirstRobin)



	function login(username,password) {
		if (typeof username === "string" && typeof password === "string"){
		console.log("Both arguments are strings.");
		} else{ 
		console.log("One of the arguments is not a string.");
		}
	}

	login("Adrian","RuleManila")
	login("Adrian",1234)


	function oddevenchecker(num){
		if (num % 2 === 0){
			console.log("The number is even.")
		} else {
			console.log("The number is odd.")
		}
	}

oddevenchecker(5)
oddevenchecker(6)


	function budgetchecker(num){
		if (num > 40000){
			console.log("You are over the budget.")
		} else {
			console.log("You have resources left.")
		}
	}

budgetchecker(50000)
budgetchecker(25000)


let adrian = 1

++adrian
console.log(adrian)




